import axios from 'axios'
import { Toast } from 'vant'

axios.defaults.baseURL = 'http://127.0.0.1:3000'
axios.defaults.withCredentials = true // 让ajax携带cookie
axios.interceptors.response.use(
  res => {
    if (res.data.error === 400) {
      Toast.fail(res.data.message)
      console.log(location)
      location.href = `/#/login?repath=/${location.hash}`
    }
    return res
  }
)

// 获取一级分类
export const queryTopCategory = () => axios.get('/category/queryTopCategory')

// 获取二级分类
export const querySecondCategory = (id) => axios.get('/category/querySecondCategory', { params: { id } })

// 查询历史记录
export const queryProduct = (obj) => axios.get('/product/queryProduct', { params: obj })

// 登录请求
export const login = obj => axios.post('/user/login', obj)

// 登出请求
export const logout = () => axios.get('/user/logout')

// 注册请求
export const register = obj => axios.post('/user/register', obj)

// 发送验证码
export const vcode = () => axios.get('/user/vCode')

// 商品详情页
export const getProductDetail = (id) => axios.get('/product/queryProductDetail', { params: { id } })
